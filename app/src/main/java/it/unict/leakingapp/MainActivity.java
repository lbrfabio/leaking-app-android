package it.unict.leakingapp;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by misael on 31/07/2014
 * Ported to android by fabiolbr
 */
public class MainActivity extends Activity {

    private TextView unauthorizedTextField;
    private TextView authorizedTextField;
    private Button getSensitiveButton;
    private Button getNonSensitiveButton;
    private Button sendButton;
    //private TextView labelStatus;
    private RadioButton unauthorizedRadioButton;
    private RadioButton authorizedRadioButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSensitiveButton = (Button) findViewById(R.id.getSensitiveButton);
        getNonSensitiveButton = (Button) findViewById(R.id.getNonSensitiveButton);
        sendButton = (Button) findViewById(R.id.sendButton);
        unauthorizedTextField = (TextView) findViewById(R.id.unauthorizedTextField);
        authorizedTextField = (TextView) findViewById((R.id.authorizedTextField));
        unauthorizedRadioButton = (RadioButton) findViewById(R.id.unauthorizedRadioButton);
        authorizedRadioButton = (RadioButton) findViewById(R.id.authorizedRadioButton);
        //labelStatus = (TextView) findViewById(R.id.labelStatus);

        API.init(this, "sensitive_data.txt", "unsensitive_data.txt", unauthorizedTextField, authorizedTextField);

        getSensitiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                App.data = API.loadSensitive();
                //labelStatus.setText("Sensitive data loaded");
                Toast.makeText(getApplicationContext(), "Sensitive data loaded", Toast.LENGTH_SHORT).show();
            }
        });

        getNonSensitiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                App.data = API.loadUnsensitive();
                //labelStatus.setText("Non-sensitive data loaded");
                Toast.makeText(getApplicationContext(), "Non-sensitive data loaded", Toast.LENGTH_SHORT).show();
            }
        });

        authorizedRadioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                App.outChannel = new SendAuthorized();
            }
        });

        unauthorizedRadioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                App.outChannel = new SendUnauthorized();
            }
        });


        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (App.outChannel == null) {   // to avoid crash
                    Toast.makeText(getApplicationContext(), "Output Channel not Selected", Toast.LENGTH_SHORT).show();
                } else if (App.data == 0) {     // to avoid loading the default value of App.data
                    Toast.makeText(getApplicationContext(), "Data not Loaded", Toast.LENGTH_SHORT).show();
                } else {
                    App.outChannel.Send(App.data);
                }
            }
        });

    }


    /**
     * Created by misael on 18/07/2014
     */
    public class SendAuthorized extends ISend {
        @Override
        public void Send(int data) {
            API.sendAuthorized(data);
        }
    }

    public class SendUnauthorized extends ISend {
        @Override
        public void Send(int data) {
            API.sendUnauthorized(data);
            //java.lang.System.out.println(data);
        }
    }
}