package it.unict.leakingapp;

import android.content.Context;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by misael on 31/07/2014
 * Ported to android by fabiolbr
 */
public class API {
    static String sensitiveDataFile;
    static String unsensitiveDataFile;
    static TextView unauthorizedTextField;
    static TextView authorizedTextField;
    private static Context appContext;

    public static void init(Context context, String _sensitiveDataFile, String _unsensitiveDataFile, TextView _unauthorizedTextField, TextView _authorizedTextField) {
        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.
        // See this article for more information: http://bit.ly/6LRzfx
        appContext = context.getApplicationContext();
        sensitiveDataFile = _sensitiveDataFile;
        unsensitiveDataFile = _unsensitiveDataFile;
        unauthorizedTextField = _unauthorizedTextField;
        authorizedTextField = _authorizedTextField;
    }

    public static int loadSensitive() {
        int data = 0;
        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(appContext.getAssets().open(sensitiveDataFile)));
            try {
                String line = br.readLine();
                data = Integer.parseInt(line);
            } finally {
                br.close();
            }
        } catch (IOException e) {
            Toast.makeText(appContext, "Error in reading the file." + e.getMessage(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
        return data;
    }

    public static int loadUnsensitive() {
        int data = 0;
        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(appContext.getAssets().open(unsensitiveDataFile)));
            try {
                String line = br.readLine();
                data = Integer.parseInt(line);
            } finally {
                br.close();
            }
        } catch (IOException e) {
            Toast.makeText(appContext, "Error in reading the file." + e.getMessage(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
        return data;
    }

    public static void sendAuthorized(int data) {
        authorizedTextField.setText("" + data);
    }

    public static void sendUnauthorized(int data) { unauthorizedTextField.setText("" + data); }

}
